using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;


public class goalMove : MonoBehaviour {

	// Use this for initialization
	List<string> directions;
	string direction;
	private float nextActionTime;
	private float period = 0.1f;
	//Health vars
	private float maxhealth = 10.0f;
	private float regenRate = 1.5f;
	public float curHealth;
	private bool isRegen = false;
	//Text vars
	private Text healthText;


	void Awake() {
		directions = new List<string>();
		directions.Add ("up");
		directions.Add ("down");
		directions.Add ("left");
		directions.Add ("right");
		nextActionTime = Time.time;
		curHealth = maxhealth;
		healthText = GameObject.Find ("healthText").GetComponent<Text>();
		healthText.text = curHealth.ToString();
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//GameObject actorObj = (GameObject)GameObject.Instantiate(Resources.Load("goal"));
		//Collider wallTest = Physics.OverlapBox(new Vector3(transform.position.x, 0.0f, transform.position.z), new Vector3(0.3f, 0.3f, 0.3f));
		/*
		if (Time.time > nextActionTime) {
			nextActionTime = Time.time + period;
			direction = directions [(int)Random.Range(0, directions.Count)];
		}

		Vector3 newPosition = new Vector3();
		if (direction == "up") {
			newPosition = new Vector3(transform.position.x, 0.0f, transform.position.z + 0.5f);
		} else if (direction == "down") {
			newPosition = new Vector3(transform.position.x, 0.0f, transform.position.z - 0.5f);
		} else if (direction == "left") {
			newPosition = new Vector3(transform.position.x + 0.5f, 0.0f, transform.position.z);
		} else if (direction == "right") {
			newPosition = new Vector3(transform.position.x - 0.5f, 0.0f, transform.position.z);
		}*/

		Vector3 newPosition = new Vector3();
		if (Input.GetKey(KeyCode.D)) {
			newPosition = new Vector3(transform.position.x, 0.0f, transform.position.z + 0.5f);
		} else if (Input.GetKey(KeyCode.A)) {
			newPosition = new Vector3(transform.position.x, 0.0f, transform.position.z - 0.5f);
		} else if (Input.GetKey(KeyCode.S)) {
			newPosition = new Vector3(transform.position.x + 0.5f, 0.0f, transform.position.z);
		} else if (Input.GetKey(KeyCode.W)) {
			newPosition = new Vector3(transform.position.x - 0.5f, 0.0f, transform.position.z);
		}

		Collider[] wallTest = Physics.OverlapBox (newPosition, new Vector3 (0.5f, 0.5f, 0.5f));
		if (wallTest.Where (col => col.gameObject.tag == "wall").ToArray ().Length == 0) {
			transform.position = newPosition;
		}


//		if (curHealth != maxhealth && !isRegen) {
//			StartCoroutine (regenHealth ());
//		}

		healthText.text = curHealth.ToString();

	}

	private IEnumerator regenHealth(){
		isRegen = true;
		while (curHealth < maxhealth) {
			updateHealth (0.25f);
			yield return new WaitForSeconds (regenRate);
		}
		isRegen = false;
	}

	//negative values damage, postive values heal player 
	public void updateHealth(float damage){
		this.curHealth += damage;
	}
}
